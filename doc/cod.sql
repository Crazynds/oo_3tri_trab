
DROP TABLE venda_produto CASCADE CONSTRAINTS;
DROP TABLE venda CASCADE CONSTRAINTS;
DROP TABLE cliente CASCADE CONSTRAINTS;
DROP TABLE produto CASCADE CONSTRAINTS;

DROP SEQUENCE produto_id;
DROP SEQUENCE venda_id;

CREATE SEQUENCE produto_id;
CREATE SEQUENCE venda_id;
/* Create Tables */

CREATE TABLE cliente
(
	nome varchar2(100),
	cpf_cnpj varchar2(20) NOT NULL,
	PRIMARY KEY (cpf_cnpj)
);


CREATE TABLE produto
(
	id number NOT NULL,
	nome varchar2(100),
	desconto float,
	valor float NOT NULL,
	vendido varchar2(1) DEFAULT 'n',
	PRIMARY KEY (id)
);

CREATE TABLE venda
(
	id number NOT NULL,
	cpf_cnpj varchar2(20) NOT NULL,
	valor_pago float,
	valor_total float,
	desconto_loja float,
	PRIMARY KEY (id)
);


CREATE TABLE venda_produto
(
	id_produto number NOT NULL,
	id_venda number NOT NULL
);



/* Create Foreign Keys */

ALTER TABLE venda
	ADD FOREIGN KEY (cpf_cnpj)
	REFERENCES cliente (cpf_cnpj)
;


ALTER TABLE venda_produto
	ADD FOREIGN KEY (id_produto)
	REFERENCES produto (id)
;


ALTER TABLE venda_produto
	ADD FOREIGN KEY (id_venda)
	REFERENCES venda (id)
;
