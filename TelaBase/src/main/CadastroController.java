package main;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;


public class CadastroController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField qtd;
    @FXML
    private TextField valor;
    @FXML
    private TextField desc;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void cadastra(ActionEvent event) {
        try{
            int y=Integer.valueOf(qtd.getText());
            for (int x=0;x<y;x++){
                Produto p=new Produto();
                p.setNome(nome.getText());
                p.setDesc(Float.valueOf(desc.getText()));
                p.setValor(Float.valueOf(valor.getText()));
                p.insert();
            }
            Main.escreveLog("Cadastrado "+y+" produtos de nome "+nome.getText());
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            Main.setScene("TelaIni.fxml");
        }
    }
    
}
