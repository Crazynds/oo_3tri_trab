package main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Venda {
    private ArrayList<Produto> p=new ArrayList<Produto>();
    
    private int id;
    float desc=0,val=0,descP=0;
    private Cupom c;
    
    public void add(Produto pd){
        p.add(pd);
        update();
    }
    
    public void remove(Produto pd){
        p.remove(pd);
        update();
    }
    
    public void insert(Cliente c){
        Conexao y=new Conexao();
        try {
            PreparedStatement ps = y.getConexao().prepareStatement("INSERT INTO venda VALUES (venda_id.next,?,?,?,?)");
            ps.setString(1,c.getCpf());
            ps.setFloat(2,getVal()-getDesc());
            ps.setFloat(3,getVal());
            ps.setFloat(4,getDesc());
            ps.execute();
            ResultSet rs=y.getConexao().prepareStatement("select * from venda where id=(select max(id) from venda)").executeQuery();
            if(rs.next()){
                id=rs.getInt(1);
            }
            
        }catch(Exception e){}finally{
            y.desconecta();
        }
        y=new Conexao();
        for(Produto t:p){
            try {
                PreparedStatement ps = y.getConexao().prepareStatement("INSERT INTO venda_produto VALUES (?,?)");
                ps.setInt(1, t.getCod());
                ps.setInt(2, id);
                ps.execute();
            }catch(Exception e){}finally{
                y.desconecta();
            }
        }
    }
    
    private void update(){
        val=0;
        descP=0;
        desc=0;
        for(Produto c:p){
            val+=c.getValor();
            descP+=c.getDesc();
        }
        if(c!=null)switch(c.getFormaDesconto()){
            case "valor":
                desc=Integer.valueOf(c.getDesconto());
                break;
            case "porcentagem":
                float valorP=Integer.valueOf(c.getDesconto());
                valorP/=100;
                desc=val*valorP;
                break;
            case "nItens":
                float valorN=Integer.valueOf(c.getDesconto());
                desc=(p.size()*valorN);
                break;
            case "maiorValor":
                desc=0;
                for(Produto c:p){
                    if(c.getValor()-c.getDesc()>desc)desc=c.getValor()-c.getDesc();
                }
                break;
            case "menorValor":
                desc=0;
                for(Produto c:p){
                    if(c.getValor()-c.getDesc()<desc || p.indexOf(c)==0)desc=c.getValor()-c.getDesc();
                }
                break;
        }else{
            desc=0;
        }
    }
    
    public void setDesc(float d){
        desc=d;
    }
    
    public float getVal(){        
        return val-desc;
    }
    
    public void setCupom(Cupom c){
        this.c=c;
        update();
    }
    
    public float getDesc(){
        return descP;
    }
    
    public float getRealVal(){
        return getVal()-getDesc();
    }
    
    public int size(){
        return p.size();
    }
    
    public Cupom getCupom(){
        return c;
    }
    
}
