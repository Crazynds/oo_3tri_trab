package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private File f;
    private FileWriter fw;

    public Logger(int id) {
        f = new File("log venda="+id+".txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(String str) {
        try {
             fw.append("\r\n { data : '" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+ "', registro: '"+ str +"'},");
             fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize(){
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //super.finalize(); 
    }
}