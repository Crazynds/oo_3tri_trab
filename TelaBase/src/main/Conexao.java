package main;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexao {

    private String usuario = "bd3_int07";
    private String senha = "bd3_int07";
    private final String servidor = "oracle.canoas.ifrs.edu.br";
    private final int porta = 1521;

    private Connection conexao = null;
    
    public Conexao(){}

    public Conexao(String usuario,String senha) {
        this.senha = senha;
        this.usuario = usuario;
    }

    public Connection getConexao() {
        if (conexao == null) {
            try {
                conexao = DriverManager.getConnection("jdbc:oracle:thin:@" + this.servidor + ":" + this.porta + ":XE",this.usuario,this.senha);
            } catch(SQLException e){
                e.printStackTrace();
            }

        }
        return conexao;
    }

    public void desconecta() {
        try {
            conexao.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
