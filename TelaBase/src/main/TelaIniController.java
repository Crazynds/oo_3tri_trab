/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 *
 * @author Aluno
 */
public class TelaIniController implements Initializable {
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void venda(ActionEvent event) {
        Main.setScene("Venda.fxml");
    }

    @FXML
    private void cadastra(ActionEvent event) {
        Main.setScene("Cadastro.fxml");
    }

    @FXML
    private void cadastraCli(ActionEvent event) {
        Main.setScene("CadastraCli.fxml");
    }
    
}
