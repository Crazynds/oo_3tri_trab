package main;

import java.net.URL;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Venda2Controller implements Initializable {
    
    private static Cliente c;
    @FXML
    private TableView<Produto> listCar;
    @FXML
    private TableColumn<Produto, ?> produtoCar;
    @FXML
    private TableColumn<Produto, ?> valorCar;
    @FXML
    private TableView<Produto> listLoja;
    @FXML
    private TableColumn<Produto, ?> produtoLoja;
    @FXML
    private TableColumn<Produto, ?> valorLoja;
    @FXML
    private Label valor;
    
    private Venda v=new Venda();
    private boolean cup=false;
    @FXML
    private TextField cupom;
    @FXML
    private Label cupNota;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        produtoCar.setCellValueFactory(new PropertyValueFactory<>("nome"));
        produtoLoja.setCellValueFactory(new PropertyValueFactory<>("nome"));
        valorLoja.setCellValueFactory(new PropertyValueFactory<>("valor"));
        valorCar.setCellValueFactory(new PropertyValueFactory<>("valor"));
        update();
        
        Main.newArq();
    }    
    
    public void update(){
        listLoja.getItems().clear();
        ObservableList<Produto> o=listLoja.getItems();
        ArrayList<Produto> c=Produto.getAll();
        for(Produto e:c){
            o.add(e);
        }
        listLoja.setItems(o);
        
        valor.setText("R$"+v.getRealVal());
    }
    
    
    public static void setCliente(Cliente c){
        Venda2Controller.c=c;
    }

    @FXML
    private void inclui(ActionEvent event) {
        Produto p=listLoja.getSelectionModel().getSelectedItem();
        if(p==null)return;
        p.setVend(true);
        p.update();
        listCar.getItems().add(p);
        listLoja.getItems().remove(p);
        v.add(p);
        update();
        Main.escreveLog("Removido produto "+p.getNome()+" ao carrinho de "+c.getNome());
    }

    @FXML
    private void exclui(ActionEvent event) {
        Produto p=listCar.getSelectionModel().getSelectedItem();
        if(p==null)return;
        p.setVend(false);
        p.update();
        listLoja.getItems().add(p);
        listCar.getItems().remove(p);
        v.remove(p);
        update();
        Main.escreveLog("Removido produto "+p.getNome()+" ao carrinho de "+c.getNome());
    }

    @FXML
    private void finalizar(ActionEvent event) {
        v.insert(c);
        v.getCupom().setCupon(v);
        Main.escreveLog("Vendido "+v.size()+" produtos para "+c.getNome()+" no total de R$"+v.getRealVal());
        Main.close();
        Main.setScene("TelaIni.fxml");
    }

    @FXML
    private void setCupom(ActionEvent event) {
        if(cup)return;
        Cupom c=Cupom.getCupom();
        v.setCupom(c);
        cup=true;
        cupom.setText(c.getCodigo());
        cupNota.setText("Voce ganhou um desconto de "+c.getDesconto()+" na forma de "+c.getFormaDesconto());
        update();
    }
    
}
