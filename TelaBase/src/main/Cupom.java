package main;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class Cupom {
    private String codigo,desconto,formaDesconto;
    
    public static Cupom getCupom(){
        WebService wc = new WebService();
        String res="";
        try{
            res=wc.getContentAsString("http://www.khonsu.cf/webService/geraCupomDesconto?nome=LuizHenrique&vendaJSON={id:5,%27venda%27]}");
        }catch(Exception e){
            e.printStackTrace();
        }
        Gson g =new Gson();
        Cupom c=g.fromJson(res, new TypeToken<Cupom>(){}.getType());
        System.out.println("Codigo: "+c.codigo+"-desc:"+c.desconto+"-forma:"+c.formaDesconto);
        return c;
    }
    
    public void setCupon(Venda v){
        WebService wc = new WebService();
        try{
            wc.getContentAsString("http://www.khonsu.cf/webService/geraCupomDesconto?nome=LuizHenrique&vendaJSON={http://www.khonsu.cf/webService/salvaVenda?nome=LuizHenrique&venda={id:5,cupomDesconto:%20%27"+codigo+"%27,venda:"+v.getRealVal()+"}}");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDesconto() {
        return desconto;
    }

    public void setDesconto(String desconto) {
        this.desconto = desconto;
    }

    public String getFormaDesconto() {
        return formaDesconto;
    }

    public void setFormaDesconto(String formaDesconto) {
        this.formaDesconto = formaDesconto;
    }
    
    
    
    
    
    
    
}
