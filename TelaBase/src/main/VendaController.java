package main;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class VendaController implements Initializable {

    @FXML
    private TableView<Cliente> tabela;
    @FXML
    private TableColumn<Cliente, ?> nome;
    @FXML
    private TableColumn<Cliente, ?> cpf;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        cpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));
        tabela.getItems().clear();
        ObservableList<Cliente> o=tabela.getItems();
        ArrayList<Cliente> c=Cliente.getAll();
        for(Cliente e:c){
            o.add(e);
        }
        tabela.setItems(o);
        
    }    

    @FXML
    private void continuar(ActionEvent event) {
        Cliente c=tabela.getSelectionModel().getSelectedItem();
        if(c==null)return;
        Venda2Controller.setCliente(c);
        Main.setScene("Venda2.fxml");
    }

    @FXML
    private void voltar(ActionEvent event) {
        Main.setScene("TelaIni.fxml");
    }
    
}
