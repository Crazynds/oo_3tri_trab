
package main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Cliente {
    private String nome,cpf;
    
    private Cliente(String nome,String cpf){
        this.nome=nome;
        this.cpf=cpf;
    }
    
    public Cliente(){}
    
    public static Cliente getCliente(String cpf){
        Cliente u;
        Conexao c=new Conexao();
        try{
            PreparedStatement ps=c.getConexao().prepareStatement("SELECT * FROM cliente WHERE cpf=?");
            ps.setString(1, cpf);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                u=new Cliente();
                u.nome=rs.getString(1);
            }else{
                u=null;
            }
        }catch(Exception e){
            e.printStackTrace();
            u=null;
        }finally{
            c.desconecta();
        }
        return u;
    }
    
    public static ArrayList<Cliente> getAll(){
        Conexao c=new Conexao();
        ArrayList<Cliente> al=new ArrayList<>();
        try {
            ResultSet rs=c.getConexao().prepareStatement("SELECT * FROM cliente").executeQuery();
            while(rs.next())al.add(new Cliente(rs.getString(1),rs.getString(2)));
        }catch(Exception ex){ex.printStackTrace();return null;}finally{
            c.desconecta();
        }
        return al;
    }
    
    public boolean insert(){
        Conexao c=new Conexao();
        try {
            PreparedStatement ps = c.getConexao().prepareStatement("INSERT INTO cliente VALUES (?,?)");
            ps.setString(1,nome);
            ps.setString(2, cpf);
            ps.execute();
        }catch(Exception e){return false;}finally{
            c.desconecta();
        }
        return true;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    
}
