/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class CadastraCliController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField cpf;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Main.newArq();
    }    

    @FXML
    private void cadastra(ActionEvent event) {
        try{
            Cliente c=new Cliente();
            c.setCpf(cpf.getText());
            c.setNome(nome.getText());
            c.insert();
            Main.escreveLog("Cadastrado cliente de cpf"+cpf.getText());
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            Main.setScene("TelaIni.fxml");
        }
        Main.close();
    }
    
}
