package main;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage s;
    private static Logger l;
    private static int arq=1;
    
    public static void setScene(String local){
        Parent root;
        try {
            root = FXMLLoader.load(Main.class.getResource(local));
            Scene scene = new Scene(root);
            s.setScene(scene);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void newArq(){
        l=new Logger(arq++);
    }
    
    public static void close(){
        l.finalize();
    }
    
    public static void escreveLog(String s){
        l.escreve(s);
    }

    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TelaIni.fxml"));
        
        Scene scene = new Scene(root);
        s=stage;
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
