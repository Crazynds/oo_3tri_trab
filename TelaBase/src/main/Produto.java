package main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Produto {
    private String nome;
    private float valor,desc;
    private int cod;
    private boolean ven=false;
    
    public Produto(){}
    
    private Produto(String nome,int cod,float valor,float desc,boolean ven){
        this.nome=nome;
        this.cod=cod;
        this.valor=valor;
        this.desc=desc;
        this.ven=ven;
    }
    
    public static Produto getProduto(int cod){
        Produto u;
        Conexao c=new Conexao();
        try{
            PreparedStatement ps=c.getConexao().prepareStatement("SELECT * FROM produto WHERE cod=?");
            ps.setInt(1, cod);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                u=new Produto();
                u.cod=rs.getInt(1);
                u.nome=rs.getString(2);
                u.valor=rs.getFloat(3);
                u.desc=rs.getFloat(4);
                u.ven=!rs.getString(5).equals("n");
            }else{
                u=null;
            }
        }catch(Exception e){
            e.printStackTrace();
            u=null;
        }finally{
            c.desconecta();
        }
        return u;
    }
    
    public static ArrayList<Produto> getAll(){
        Conexao c=new Conexao();
        ArrayList<Produto> al=new ArrayList<>();
        try {
            ResultSet rs=c.getConexao().prepareStatement("select * from produto where vendido='n'").executeQuery();
            while(rs.next()){
                Produto p=new Produto(rs.getString(2),rs.getInt(1),rs.getFloat(4),rs.getFloat(3),false);
                al.add(p);
            }
        }catch(Exception ex){ex.printStackTrace();return null;}finally{
            c.desconecta();
        }
        return al;
    }
    public static ArrayList<Produto> getAll(String nome){
        Conexao c=new Conexao();
        ArrayList<Produto> al=new ArrayList<>();
        try {
            ResultSet rs=c.getConexao().prepareStatement("SELECT * FROM produto WHERE vendido='n' and nome like '%"+nome+"%'").executeQuery();
            while(rs.next())al.add(new Produto(rs.getString(1),rs.getInt(2),rs.getFloat(3),rs.getFloat(4),!rs.getString(5).equals("n")));
        }catch(Exception ex){ex.printStackTrace();return null;}finally{
            c.desconecta();
        }
        return al;
    }
    
    public boolean insert(){
        Conexao c=new Conexao();
        try {
            PreparedStatement ps = c.getConexao().prepareStatement("INSERT INTO produto VALUES (produto_id.nextval,?,?,?,?)");
            ps.setString(1,nome);
            ps.setFloat(3, valor);
            ps.setFloat(2, desc);
            ps.setString(4, "n");
            ps.execute();
        }catch(Exception e){e.printStackTrace();return false;}finally{
            c.desconecta();
        }
        return true;
    }
    public boolean update(){
        Conexao c=new Conexao();
        try {
            PreparedStatement ps = c.getConexao().prepareStatement("UPDATE produto SET desconto=?,vendido=?  WHERE id=?");
            ps.setFloat(1, desc);
            ps.setString(2, (ven)?"s":"n");
            ps.setInt(3, cod);
            ps.execute();
        }catch(Exception e){e.printStackTrace();return false;}finally{
            c.desconecta();
        }
        return true;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getDesc() {
        return desc;
    }

    public void setDesc(float desc) {
        this.desc = desc;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
    
    public void setVend(boolean b){
        ven=b;
    }
}
